const mongoose = require("mongoose");

const company = mongoose.Schema({
	name: String,
    balances: [
        {
			jahr: Number,
			anlagevermoegen: Number,
			umlaufvermoegen: Number,
			eigenkapital: Number,
			fremdkapital: {
				kurzfristig: Number,
				langfristig: Number
			}
        }
    ]
});

module.exports = mongoose.model("Company", company);
