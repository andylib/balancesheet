const express = require("express");
const Company = require("./models/Company");
const router = express.Router();
const calculations = require("./calc/calculations");
const mongoose = require("mongoose");

router.get("/", async (req, res) => res.send({message: "welcome"}));

router.get("/company", async (req, res) => {
	const company = await Company.find();
	res.send(company);
});

router.get("/company/:id", async (req, res) => {
	const id = req.params.id;

	if (mongoose.isValidObjectId(id)) {
		const company = await Company.findById(id);
		res.send(company);
	}
	else {
		res.status(400).send({message: "invalid id"});
	}
});

// add company
router.post("/company", async (req, res) => {
	const danger = req.body.name.includes("<", ">", "/");
	if(danger){
		res.status(400).send({message: "no game"});
		return;
	}
	const company = new Company(req.body);
	await company.save();
	res.send(company);
});

// edit balance of company
router.put("/company/:id", async (req, res) => {
	const id = req.params.id;
	const body = req.body;
	const company = await Company.findByIdAndUpdate(id, body);
	res.send(company);
});


// calculations

router.get("/calc", async (req, res) => res.send({message: "choose a company"}));

router.get("/calc/:id", async (req, res) => {
	const id = req.params.id;
	const company = await Company.findById(id);
	const { balances, name } = company;

	let final = {name, figures: []}

	balances.forEach(balance => {
		const {
			jahr, anlagevermoegen, umlaufvermoegen, eigenkapital, fremdkapital
		} = balance;
		const { kurzfristig, langfristig} = fremdkapital;
		const fk = kurzfristig + langfristig;

		const figure = {
			jahr,
			eigenkapitalquote: calculations.eigenkapitalQuote(eigenkapital, fk),
			fremdkapitalquote: calculations.fremdkapitalQuote(eigenkapital, fk),
			verschuldungsgrad: calculations.verschuldungsgrad(eigenkapital, fk),
			anlageintensität: calculations.anlagenintensität(anlagevermoegen, umlaufvermoegen),
			umlaufintensität: calculations.umlaufintensität(anlagevermoegen, umlaufvermoegen),
			deckungsgrad1: calculations.deckungsgrad1(anlagevermoegen, eigenkapital),
			deckungsgrad2: calculations.deckungsgrad2(anlagevermoegen, eigenkapital, langfristig),
			liquditaet3grad: calculations.liquiditaet3grad(umlaufvermoegen, kurzfristig),
			workingCapital: calculations.workingCapital(umlaufvermoegen, kurzfristig)
		};
		final.figures.push(figure);
	});
	res.send(final);
});

module.exports = router;
