const express = require("express");
const mongoose = require("mongoose");
const routes = require("./routes");
const cors = require("cors");

(async () => {
	mongoose
	.connect("mongodb://localhost:27017/balancesheet", { useNewUrlParser: true });
})();

const app = express();
app.use(cors());
app.use(express.json());
app.use("/", routes);

const PORT = 5000;
app.listen(PORT, () => {
	console.log(`server running on ${PORT}`);
});
