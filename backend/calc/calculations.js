const roundToFour = num => +(Math.round(num + "e4")  + "e-4");

const calculations = {
    eigenkapitalQuote: (ek, fk) => roundToFour(ek / (ek + fk)),
    fremdkapitalQuote: (ek, fk) => roundToFour(fk / (ek + fk)),
    verschuldungsgrad: (ek, fk) => roundToFour(fk / ek),
    anlagenintensität: (av, uv) => roundToFour(av / (av + uv)),
    umlaufintensität: (av, uv) => roundToFour(uv / (av + uv)),
    deckungsgrad1: (av, ek) => roundToFour(ek / av),
    deckungsgrad2: (av, ek, lfk) => roundToFour((ek + lfk) / av),
    liquiditaet3grad: (uv, kfk) => roundToFour(uv / kfk),
    workingCapital: (uv, kfk) => roundToFour(uv - kfk)
}

module.exports = calculations;
