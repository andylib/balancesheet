import "../css/style.css";
import Navigo from "navigo";
import { main } from "./main";
import { view } from "./view";
import { edit } from "./edit";

const router = new Navigo("/");
const app = document.querySelector<HTMLDivElement>("main")!;

router.on("/", () => main(app))
.on("/view", () => window.location.href = "/")
.on("/view/:id", () => view(app))
.on("/edit", () => window.location.href = "/")
.on("/edit/:id", () => edit(app))
.on("/**", () => window.location.href = "/")
.resolve();
