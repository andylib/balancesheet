import "../css/view.css";
import { NumberInterpretation } from "./wcp";
import { URL } from "./URL";

export const view = (app: HTMLDivElement): void => {
    app.innerHTML = build();
    customElements.define("num-intrpr", NumberInterpretation);
    (async () => await buildFunctionality())();
}

const build = (): string => {
    return `
        <h2 id="name"></h2>
        <a id="edit" href="/">edit</a>
        <div id="balance">
            <h2>balancesheet</h2>
            <div class="table">
                <div class="col">
                    <p>Jahr</p>
                    <p>Anlagevermögen</p>
                    <p>Umlaufvermögen</p>
                    <p>Eigenkapital</p>
                    <p>kurzfristiges Fremdkapital</p>
                    <p>langfristiges Fremdkapital</p>
                </div>
            </div>
        </div>
        <div id="figures">
            <h2>figures</h2>
            <div class="table">
                <div class="col">
                    <p>Jahr</p>
                    <p>Eigenkapitalquote</p>
                    <p>Fremdkapitalquote</p>
                    <p>Verschuldungsgrad</p>
                    <p>Anlageintensität</p>
                    <p>Umlaufintensität</p>
                    <p>Deckungsgrad 1</p>
                    <p>Deckungsgrad 2</p>
                    <p>Liquidität 3. Grades</p>
                    <p>Working Capital</p>
                </div>
            </div>
        </div>
    `;
}

const fetchAPI = async (url: string): Promise<Company> => {
    const id = location.pathname.split("/")[2];
    const response: Response = await fetch(`${url}/${id}`);
    const data: Company = await response.json();
    return data;
}

const buildFunctionality =  async (): Promise<void> => {
    const company = await fetchAPI(`${URL}/company`);
    document.querySelector<HTMLHeadingElement>("#name")!.innerHTML = company.name; 
    company.balances.forEach(balance => {
        document.querySelectorAll(".table")[0].innerHTML += `
            <div class="col">
                <p>${balance.jahr}</p>
                <p>${balance.anlagevermoegen}</p>
                <p>${balance.umlaufvermoegen}</p>
                <p>${balance.eigenkapital}</p>
                <p>${balance.fremdkapital.kurzfristig}</p>
                <p>${balance.fremdkapital.langfristig}</p>
            </div>
        `;
    });
    
    if (company.balances.length) {
        const data = await fetchAPI(`${URL}/calc`);
        data.figures!.forEach(figure => {
            document.querySelectorAll(".table")[1].innerHTML += `
                <div class="col">
                    <p>${figure.jahr}</p>
                    <num-intrpr number="${figure.eigenkapitalquote}" min="0.3" max="100"></num-intrpr>
                    <num-intrpr number="${figure.fremdkapitalquote}" min="0" max="0.5"></num-intrpr>
                    <num-intrpr number="${figure.verschuldungsgrad}" min="0" max="2"></num-intrpr>
                    <num-intrpr number="${figure.anlageintensität}" min="0.4" max="0.7"></num-intrpr>
                    <num-intrpr number="${figure.umlaufintensität}" min="0.45" max="0.55"></num-intrpr>
                    <num-intrpr number="${figure.deckungsgrad1}" min="0.7" max="10"></num-intrpr>
                    <num-intrpr number="${figure.deckungsgrad2}" min="1" max="10"></num-intrpr>
                    <num-intrpr number="${figure.liquditaet3grad}" min="1" max="1.2"></num-intrpr>
                    <num-intrpr number="${figure.workingCapital}" min="1" max="100000"></num-intrpr>
                </div>
            `;
        });
    }

    const aTag = document.querySelector<HTMLAnchorElement>("#edit")!;
    aTag.href = `/edit/${company._id}`;
}
