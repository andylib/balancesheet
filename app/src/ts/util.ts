export const fetchAPI = async (url: string): Promise<Company> => {
    const id = location.pathname.split("/")[2];
    const response: Response = await fetch(`${url}/${id}`);
    const data: Company = await response.json();
    return data;
}

export const addColumn = (): void => {
    const infoColumn = document.querySelector<HTMLDivElement>(".info-column")!;
    const inputColumn: HTMLDivElement = document.createElement("div");
    inputColumn.classList.value = "input-column";
    
    Array.from(infoColumn.children).forEach(() => {  
        const input = document.createElement("input");
        input.type = "number";
        inputColumn.appendChild(input);
    });
    document.querySelector(".input-form")!.appendChild(inputColumn);
}

export const addInput = (name: string, value: string): HTMLInputElement => {
    let inputElement: HTMLInputElement = document.createElement("input");
    inputElement.type = "number";
    inputElement.name = `${name}`;
    inputElement.value = value;
    return inputElement;
}

export const matchInputToValues = (numArr: number[][], strArr: string[]) => {
    let result: Result[] = [];

    numArr.forEach(array => {
        let obj: Result = {
            year: 0,
            anlagvermoegen: 0,
            umlaufvermoegen: 0,
            eigenkapital: 0,
            kurzfristig: 0,
            langfristig: 0
        };
        array.forEach((number, index) => {
            obj[strArr[index] as keyof Result] = number;
        });
        result.push(obj);
    });

    return result;
}

export const getColumnValues = (allInputColums: NodeListOf<HTMLDivElement>) => {
    const columns = ["year", "anlagvermoegen", "umlaufvermoegen", "eigenkapital", "kurzfristig", "langfristig"];
    let rows: number[][] = [];
    
    allInputColums.forEach(column => {
        let columnValues: number[] = [];
        Array.from(column.children).forEach(input => {
           columnValues.push(+(<HTMLInputElement>input).value)
        });
        rows.push(columnValues);
    });

    return {rows, columns};
}

export const checkIfEmpty = (valueList: string[]): Boolean => {
    const notAllowed: any[] = ["", undefined, NaN, null, false];
    let value = 0;

    notAllowed.forEach(param => value = value + (+!!valueList.includes(param)));
    return (value === 1);
}
