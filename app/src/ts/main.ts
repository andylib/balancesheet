import { URL } from "./URL";

export const main = (app: HTMLDivElement): void => {
    app.innerHTML = build();
    (async () => await buildFunctionality())();
}

const build = (): string => {
    return `
        <div class="card">
            <h3>companies</h3>
            <label>
                <span>search</span>
                <input type="text" id="search" placeholder="Apple Inc.">
            </label>
            <ul></ul>
        </div>
        <div class="card">
            <h3>new</h3>
            <label>
                <span>company</span>
                <input type="text" id="new-company" placeholder="Apple Inc.">
            </label>
            <button id="save">save</button>
        </div>
    `;
}

const searchValue = async (array: Company[], event: Event): Promise<Company[]> => {
    let filtered: Company[] = [];
    array.forEach((company: Company) => {
        const target = event.target as HTMLInputElement;
        if (company.name.toLowerCase().match(target.value.toLowerCase())) {
            filtered.push(company);
        }
    });
    return filtered;
}

const buildFunctionality = async (): Promise<void> => {
    try {
        const result: Response = await fetch(`${URL}/company`);
        const data: Company[] = await result.json();

        const ul = document.querySelector<HTMLUListElement>("ul")!;
        data.forEach(company => {
            ul.innerHTML += `
                <li>
                    <a href="/view/${company._id}">${company.name}</a>
                </li>`;
        });

        const searchINP = document.querySelector<HTMLInputElement>("#search")!;
        searchINP.addEventListener("input", async event => {
            ul.innerHTML = "";
            const filtered = await searchValue(data, event);
            filtered.forEach(company => {
                ul.innerHTML += `
                    <li>
                        <a href="/view/${company._id}">${company.name}</a>
                    </li>`;
            });
        });

    } catch (error) {
        console.log(error);
        document.querySelector<HTMLUListElement>("ul")!.innerHTML += `<li>${error}</li>`;
    }

    const saveBTN = document.querySelector<HTMLButtonElement>("#save")!;
    saveBTN.addEventListener("click", async () => {
        const name: string = document.querySelector<HTMLInputElement>("#new-company")!.value;
        if (name === "" || name === null || name === undefined) {
            throw Error("name cannot be empty");
        } else {
            try {
                const post = await fetch(`${URL}/company`, {
                    method: "POST",
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify({name}),
                });
                const newCompany = await post.json();
                if (newCompany._id) {
                    window.location.href = `/edit/${newCompany._id}`;
                } else {
                    throw Error("invalid input");
                }
            } catch (error) {
                console.log(error)
                let clientMessage = document.createElement("p");
                clientMessage.style.color = "red";
                clientMessage.innerText = "something went wrong";
                document.querySelectorAll("label")[1].insertAdjacentElement("afterend", clientMessage);
            }
        }
    });
};
