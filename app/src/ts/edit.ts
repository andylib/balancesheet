import "../css/edit.css";
import { URL } from "./URL";
import { fetchAPI, addColumn, addInput, checkIfEmpty, getColumnValues, matchInputToValues } from "./util";

export const edit = (app: HTMLDivElement): void => {
    app.innerHTML = build();
    (async () => await buildFunctionality())();
}

const build = (): string => {
    return `
        <a href="/" id="cancel">cancel</a>
        <label>
            name:
            <input id="company-name" type="text" value=""></input>
        </label>
        <h2>balancesheet</h2>
        <div class="input-form">
            <div class="info-column">
                <label for="year">Jahr:</label>
                <label for="av">Anlagevermögen:</label>
                <label for="uv">Umlaufvermögen:</label>
                <label for="ek">Eigenkapital:</label>
                <label for="kfk">kurzfristiges Fremdkapital:</label>
                <label for="lfk">langfristiges Fremdkapital:</label>
            </div>
        </div>
        <button id="addButton">add column</button>
        <button id="save">save</button>
    `;
}

const buildFunctionality = async () => {
    const company: Company = await fetchAPI(`${URL}/company`);
    const inputForm = document.querySelector<HTMLDivElement>(".input-form")!;
  
    const cancel = document.querySelector<HTMLAnchorElement>("#cancel")!;
    cancel.href = `/view/${company._id}`;

    const nameInput = document.querySelector<HTMLInputElement>("#company-name")!;
    nameInput.value = company.name;

    if (company.balances.length === 0) addColumn();

    company.balances.forEach(balance => {
        let nodeList = []
        
        const year = addInput("year", balance.jahr.toString());
        const av = addInput("av", balance.anlagevermoegen.toString());
        const uv = addInput("uv", balance.umlaufvermoegen.toString());
        const ek = addInput("ek", balance.eigenkapital.toString());
        const kfk = addInput("kfk", balance.fremdkapital.kurzfristig.toString());
        const lfk = addInput("lfk", balance.fremdkapital.langfristig.toString());

        nodeList.push(year, av, uv, ek, kfk, lfk);

        let inputColumn = document.createElement("div");
        inputColumn.classList.value = "input-column";
        nodeList.forEach(node => inputColumn.appendChild(node));
        inputForm.appendChild(inputColumn);
    });

    const addBTN = document.querySelector("#addButton")!;
    addBTN.addEventListener("click", addColumn);

    const saveBTN = document.querySelector("#save")!;
    saveBTN.addEventListener("click", async () => {

        const allInputs: NodeListOf<HTMLInputElement> = document.querySelectorAll("input");
        let valueList: string[] = [];
        allInputs.forEach(input => valueList.push(input.value));

        if (checkIfEmpty(valueList)) {
            throw Error("values cannot be empty");
        }
        else {
            let balance: Balance[] = [];
            const allInputColums: NodeListOf<HTMLDivElement> = document.querySelectorAll(".input-column")!;
            const {rows, columns} = getColumnValues(allInputColums);
            const result = matchInputToValues(rows, columns);

            result.forEach(result => {
                balance.push({
                    jahr: result.jahr,
                    anlagevermoegen: result.anlagevermoegen,
                    umlaufvermoegen: result.umlaufvermoegen,
                    eigenkapital: result.eigenkapital,
                    fremdkapital: {
                        kurzfristig: result.kurzfristig,
                        langfristig: result.langfristig
                    }
                });
            });
            
            const final = {
                name: document.querySelector<HTMLInputElement>("#company-name")!.value,
                balance
            }

            try {
                const post = await fetch(`${URL}/company/${company._id}`, {
                    method: 'PUT',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(final),
                });
                const response = await post.json();
                if (response._id) {
                    window.location.href = `/view/${company._id}`;
                } else {
                    throw Error("invalid input");
                }
            } catch (error) {
                console.log(error);
            }
        }
    });
}
