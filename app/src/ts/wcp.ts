export class NumberInterpretation extends HTMLElement {
    _style = document.createElement("style");
    _template = document.createElement("template");
    number: string;
    min: string;
    max: string;

    constructor() {
        super();
        this.number = this.getAttribute("number")!;
        this.min = this.getAttribute("min")!;
        this.max = this.getAttribute("max")!;
        this._defineStyle();
        this._defineTemplate();
        this._attachToShadowDOM();
    }

    _defineStyle() {
        this._style.innerHTML = `p { margin: 0; display: flex; justify-content: center; align-items: center; }`;
    }

    _defineTemplate() {
        let styling = "color: var(--negative-color); background-color: var(--negative-background);";
        if (+this.number >= +this.min && +this.number <= +this.max){
            styling = "color: var(--positive-color); background-color: var(--positive-background);"
        }
        this._template.innerHTML = `<p style="${styling}">${this.number}</p>`;
    }

    _attachToShadowDOM() {
        this.attachShadow({ mode: "open" });
        this.shadowRoot!.appendChild(this._style);
        this.shadowRoot!.appendChild(this._template.content.cloneNode(true));
    }
}
