/// <reference types="vite/client" />

interface Company {
    _id: string;
    name: string;
    balances: Balance[];
    figures?: Figure[];
}

interface Balance {
    jahr: number;
    anlagevermoegen: number;
    umlaufvermoegen: number;
    eigenkapital: number;
    fremdkapital: {
        kurzfristig: number;
        langfristig: number;
    }
}

interface Figure {
    jahr: number;
    eigenkapitalquote: number;
    fremdkapitalquote: number;
    verschuldungsgrad: number;
    anlageintensität: number;
    umlaufintensität: number;
    deckungsgrad1: number;
    deckungsgrad2: number;
    liquditaet3grad: number;
    workingCapital: number;
}

interface Result {
    jahr: number;
    anlagevermoegen: number;
    umlaufvermoegen: number;
    eigenkapital: number;
    kurzfristig: number;
    langfristig: number;
}
