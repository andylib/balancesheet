import { checkIfEmpty, matchInputToValues } from "../util";

test("check if any input is empty", () => {
    const strArr: string[] = ["", " ", "&nbsp;"];
    expect(checkIfEmpty(strArr)).toBe(true);
});

test("check if not empty", () => {
    const strArr: string[] = ["2020", "2021"];
    expect(checkIfEmpty(strArr)).toBe(false);
});

test("column values", () => {
    const columns = ["year", "anlagvermoegen", "umlaufvermoegen", "eigenkapital", "kurzfristig", "langfristig"];
    const values: any[] | NodeListOf<HTMLDivElement> = [
        [1, 2, 3, 4, 5, 6], [3, 4, 5, 6, 7, 9]
    ];
    const expected: Result[] = [
        {
            year: 1,
            anlagvermoegen: 2,
            umlaufvermoegen: 3,
            eigenkapital: 4,
            kurzfristig: 5,
            langfristig: 6
        },
        {
            year: 3,
            anlagvermoegen: 4,
            umlaufvermoegen: 5,
            eigenkapital: 6,
            kurzfristig: 7,
            langfristig: 9
        }
    ];
    expect(matchInputToValues(values, columns)).toEqual(expected);
});
