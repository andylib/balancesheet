## balancesheet

### installation requirements
1. NodeJs
- install [nodejs LTS](https://nodejs.org)
- in your terminal run `node -v`
- should return `v16.14.0` or higher
2. MongoDB
- install [mongodb community](https://docs.mongodb.com/manual/administration/install-community/) locally
3. Project
- clone via ssh: `git clone git@gitlab.com:andylib/balancesheet.git`
- or clone via http: `git clone https://gitlab.com/andylib/balancesheet.git`

### how to get it running
open two terminal windows, in the first enter:
- `cd path/to/balancesheet/backend`
- `npm i`
- `npm run dev`

in the second
- `cd path/to/balancesheet/app`
- `npm i`
- `npm run dev`

open your browser on http://localhost:3000
