
link: microtech.de/erp-wiki/bilanzkennzahlen

vertikale Bilanzkennzahlen:
- Analyse der Kapitalstruktur
	- Eigenkapitalquote
	- Fremdkapitalquote
	- Statistischer Verschuldungsgrad
- Analyse der Vermögensstruktur
	- Anlagenintensität
	- Umlaufintensität

horizontale Bilanzkennzahlen:
- Deckungsgrade
- Liquiditätsgrade
- Working Capital

