# Formeln

Source:
- [finanzfluss](https://youtu.be/qie9sxCIhHM)

## Profitabilität
| Kennzahl | Berechnung | Information |
|---|---|---|
| Umsatzwachstum |
| EBITDA Marge | EBITDA / Umsatz |
| Eigenkapitalrendite | Jahresgewinn / Eigenkapital |

## Bilanzkennzahlen
| Kennzahl | Berechnung | Information |
|---|---|---|
| Eigenkapitalquote | EK / Bilanzsumme (EK + FK) |
| Gearing | (Schulden - Cash) / EK |

## Bewertungskennzahlen
| Kennzahl | Berechnung | Information |
|---|---|---|
| Marktkapitalisierung | Aktienanzahl * Aktienkurs |
| Unternehmenswert (EV: enterprise value) | Marktkapitalisierung + Schulden (FK) - liquide Mittel
| ? | EV / Umsatz | macht Unternehmen brancheninnerhalb vergleichbar |
| ? | EV / EBITDA | weiterer Vergleich mit Kostenbeachtung |
|Kurs-Gewinn-Verhältnis (KGV)| Marktkapitalisierung / Jahresgewinn | wie oft sind Investoren bereit den Gewinn des Unternehmens zu bezahlen? Wie lange müssen Investoren warten bis Investition renatabel ist? |
| KBV (Buch) | Marktkapitalisierung / EK | 
| KCV (Cashflow) | Marktkapitalisierung / operativer Cashflow |
