§ 275 Gliederung
(1) Die Gewinn- und Verlustrechnung ist in Staffelform nach dem Gesamtkostenverfahren oder dem Umsatzkostenverfahren aufzustellen. Dabei sind die in Absatz 2 oder 3 bezeichneten Posten in der angegebenen Reihenfolge gesondert auszuweisen.
(2) Bei Anwendung des Gesamtkostenverfahrens sind auszuweisen:
1. Umsatzerlöse
2. Erhöhung oder Verminderung des Bestands an fertigen und unfertigen Erzeugnissen
3. andere aktivierte Eigenleistungen
4. sonstige betriebliche Erträge
5. Materialaufwand:
a) Aufwendungen für Roh-, Hilfs- und Betriebsstoffe und für bezogene Waren
b) Aufwendungen für bezogene Leistungen
6. Personalaufwand:
a) Löhne und Gehälter
b) soziale Abgaben und Aufwendungen für Altersversorgung und für Unterstützung, davon für Altersversorgung
7. Abschreibungen:
a) auf immaterielle Vermögensgegenstände des Anlagevermögens und Sachanlagen
b) auf Vermögensgegenstände des Umlaufvermögens, soweit diese die in der Kapitalgesellschaft üblichen Abschreibungen überschreiten
8. sonstige betriebliche Aufwendungen
9. Erträge aus Beteiligungen, davon aus verbundenen Unternehmen
10. Erträge aus anderen Wertpapieren und Ausleihungen des Finanzanlagevermögens, davon aus verbundenen Unternehmen
11. sonstige Zinsen und ähnliche Erträge, davon aus verbundenen Unternehmen
12. Abschreibungen auf Finanzanlagen und auf Wertpapiere des Umlaufvermögens
13. Zinsen und ähnliche Aufwendungen, davon an verbundene Unternehmen
14. Steuern vom Einkommen und vom Ertrag
15. Ergebnis nach Steuern
16. sonstige Steuern
17. Jahresüberschuss/Jahresfehlbetrag.
(3) Bei Anwendung des Umsatzkostenverfahrens sind auszuweisen:
1. Umsatzerlöse
2. Herstellungskosten der zur Erzielung der Umsatzerlöse erbrachten Leistungen
3. Bruttoergebnis vom Umsatz
4. Vertriebskosten
5. allgemeine Verwaltungskosten
6. sonstige betriebliche Erträge
7. sonstige betriebliche Aufwendungen
8. Erträge aus Beteiligungen, davon aus verbundenen Unternehmen
9. Erträge aus anderen Wertpapieren und Ausleihungen des Finanzanlagevermögens, davon aus verbundenen Unternehmen
10. sonstige Zinsen und ähnliche Erträge, davon aus verbundenen Unternehmen
11. Abschreibungen auf Finanzanlagen und auf Wertpapiere des Umlaufvermögens
12. Zinsen und ähnliche Aufwendungen, davon an verbundene Unternehmen
13. Steuern vom Einkommen und vom Ertrag
14. Ergebnis nach Steuern
15. sonstige Steuern
16. Jahresüberschuss/Jahresfehlbetrag.
(4) Veränderungen der Kapital- und Gewinnrücklagen dürfen in der Gewinn- und Verlustrechnung erst nach dem Posten "Jahresüberschuß/Jahresfehlbetrag" ausgewiesen werden.
(5) Kleinstkapitalgesellschaften (§ 267a) können anstelle der Staffelungen nach den Absätzen 2 und 3 die Gewinn- und Verlustrechnung wie folgt darstellen:
1. Umsatzerlöse,
2. sonstige Erträge,
3. Materialaufwand,
4. Personalaufwand,
5. Abschreibungen,
6. sonstige Aufwendungen,
7. Steuern,
8. Jahresüberschuss/Jahresfehlbetrag.
